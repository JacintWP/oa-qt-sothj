#include <iostream>
#include <QDirIterator>
#include <QDateTime>
#include <QFileInfo>
#include <QString>

void fileScan(QFileInfo *qfi_dir){

	std::cout<<"Directory: "<< qfi_dir->path().toStdString()<<std::endl;
	QDirIterator fileIterator(qfi_dir->path(), QDir::NoDotAndDotDot | QDir::Files | QDir::Dirs, QDirIterator::NoIteratorFlags );
	int i=0;
	while(fileIterator.hasNext() && i<11){
		
		i++;
		QFileInfo qfi_file(fileIterator.next());
		
		//QDateTime qDT(qfi_file.birthTime());
		std::cout<<"\t"<<qfi_file.size()<< " byte "/*<<qDT.toString()*/<<" "<< qfi_file.fileName().toStdString()<< 	std::endl;
	}
	
}


int main(int argc, char **argv)
{
	
	if(argc < 3 || argc > 3){
		std::cout << "Wrong number of argument! " << argc<<std::endl;
		return 1;
	}
	char *end;
	int correctInput = strtol(argv[2], &end, 10);
	
	if (*end != '\0') {
	    std::cout << "invalid number input." <<std::endl;
	    return 1;
	}



	QDirIterator dirIterator(argv[1], QDir::NoDotAndDotDot | QDir::Dirs, QDirIterator::Subdirectories);
	
	while(dirIterator.hasNext() ){
		
		QFileInfo qfi(dirIterator.next());
		fileScan(&qfi);
		
		
	}
		
		//std::cout << dirIterator.next().toStdString() << std::endl;
	


	
	return 0;
}
